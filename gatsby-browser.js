// import React from "react"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap/dist/js/bootstrap.min.js"
import "@popperjs/core/dist/umd/popper.min.js"
import "./src/styles/index.scss"

import wrapperRedux from "./src/store/store"
export const wrapRootElement = wrapperRedux
