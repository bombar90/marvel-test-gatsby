import { useState , useEffect} from "react"
import { getFetchDetails } from "../helpers/getFetchDetails"
const initialState = {
    loading : false ,
    data: []
}
export const useFetchDetails = (url) => {
   const [state, setstate] = useState(initialState);
   useEffect(() => {
    getFetchDetails(url).then(resp => {
        setstate({
            loading: false ,
            data: resp
        })        
    });       
   }, [url])
   return state;
}
