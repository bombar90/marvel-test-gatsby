import { types } from "../types/types"
const init = {
  favoritos:
    typeof window !== "undefined"
      ? JSON.parse(localStorage.getItem("favoritos")) || []
      : [],
}
export const FavoritesReducer = (state = init, action) => {
  switch (action.type) {
    case types.setFavorites:
      return {
        ...state,
        favoritos: [...state.favoritos, action.payload],
      }
    case types.delFavorites:
      return {
        ...state,
        favoritos: [...state.favoritos.filter(a => a.id !== action.payload.id)],
      }
    case types.updateFavorites:
      return {
        ...state,
        favoritos: action.payload,
      }
    default:
      return state
  }
}
