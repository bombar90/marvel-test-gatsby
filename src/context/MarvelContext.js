import React, { createContext } from "react"
import { useFetch } from "../hooks/useFetch"

export const MarvelContext = createContext(null)

export const MarvelProvider = ({ children }) => {
  const { data: characters } = useFetch(1)
  const { data: comics } = useFetch(2)
  const { data: stories } = useFetch(5)
  return (
    <MarvelContext.Provider
      value={{
        characters: characters,
        comics: comics,
        categories: ["characters", "comics", "stories"],
        stories: stories,
      }}
    >
      {children}
    </MarvelContext.Provider>
  )
}
