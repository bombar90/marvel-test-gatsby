import React from "react"
import { Layaout } from "../components/Layaout"

export default function Home() {
  return (
    <Layaout>
      <div className="row mt-3">
        <div className="col-12 text-center animate__animated animate__fadeIn">
          <h1>Welcome to your Marvels Page</h1>
          <h4>
            Here you can filter , search and get details of your favorites
            marvel characters!
          </h4>
        </div>
      </div>
      <div className="row no-gutters p-5">
        <div className="col-8 p-0">
          <img
            className="img-fluid w-100 animate__animated animate__fadeInUp"
            src="/img/banner2.jpeg"
            alt="banner2"
          />
          <img
            className="img-fluid w-100 animate__animated animate__fadeInBottomLeft"
            src="/img/banner.jpg"
            alt="banner"
          />
        </div>
        <div className="col-4 p-0">
          <img
            className="img-fluid animate__animated animate__fadeInRight h-100"
            src="/img/comic1.jpg"
            alt="banner"
          />
        </div>
      </div>
    </Layaout>
  )
}
