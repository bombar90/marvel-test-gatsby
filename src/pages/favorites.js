import React from "react"
import { useSelector } from "react-redux"
import Banner from "../components/Banner"
import { Layaout } from "../components/Layaout"
import { MarvelCard } from "../components/layout/MarvelCard"

const Favorites = () => {
  const { favoritos } = useSelector(state => state.fav)
  const renderCards = () => {
    if (favoritos.length > 0) {
      return favoritos.map((item, index) => (
        <MarvelCard key={`${item.id}-${index}`} {...item} />
      ))
    } else {
      ;<p>No hay data</p>
    }
  }

  return (
    <Layaout>
      <Banner />
      <h2 className="mt-5 text-center"> Favorites </h2>
      <hr />

      <div className="row">{renderCards()}</div>
    </Layaout>
  )
}

export default Favorites
