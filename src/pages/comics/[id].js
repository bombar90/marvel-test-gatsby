import React, { useEffect, useState } from "react"
import { CharacterList } from "../../components/details/CharacterList"
import { StoriesList } from "../../components/details/StoriesList"
import { Layaout } from "../../components/Layaout"
import { getFetch } from "../../helpers/getFetch"

const ComicsDetail = ({ params }) => {
  const id = params.id
  const [comic, setComic] = useState({})
  useEffect(() => {
    getFetch(4, id).then(resp => {
      const data = resp.find(comic => comic.id === Number(id))
      if (!data) {
        // history.push('/404');
      } else {
        setComic(data)
      }
    })
  }, [id])
  const renderDescription = () => {
    return comic.description ? (
      <p className="text-muted">{comic.description}</p>
    ) : (
      <p className="text-muted"> there is no description available </p>
    )
  }
  return (
    <Layaout>
      <div className="row mt-5">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
          <img className="img-fluid" src={comic.img_grande} alt={comic.name} />
        </div>
        <div className="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
          <h3>{comic.name}</h3>
          <hr />
          {renderDescription()}
          <hr />
          <h3>Characters Comic</h3>
          <CharacterList id={id} category="comics" />
          <hr />
          <h3>Stories</h3>
          <StoriesList id={id} category="comics" />
        </div>
      </div>
    </Layaout>
  )
}

export default ComicsDetail
