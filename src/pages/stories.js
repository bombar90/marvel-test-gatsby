import React from "react"
import Banner from "../components/Banner"
import { Layaout } from "../components/Layaout"
import { MarvelList } from "../components/layout/MarvelList"
import { useFetch } from "../hooks/useFetch"

export default function Stories() {
  const { data: stories } = useFetch(5)
  return (
    <Layaout>
      <Banner />
      <h2 className="mt-5 text-center">Marvel Stories</h2>
      <MarvelList category="stories" data={stories} />
    </Layaout>
  )
}
