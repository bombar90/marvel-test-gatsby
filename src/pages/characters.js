import React from "react"
import Banner from "../components/Banner"
import { Layaout } from "../components/Layaout"
import { MarvelList } from "../components/layout/MarvelList"
import { useFetch } from "../hooks/useFetch"

const Characters = () => {
  const { data: characters } = useFetch(1)
  return (
    <Layaout>
      <Banner />
      <h2 className="mt-5 text-center">Marvel Characters</h2>
      <MarvelList category="characters" data={characters} />
    </Layaout>
  )
}
export default Characters
