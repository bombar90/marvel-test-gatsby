import React, { useEffect, useState } from "react"
import { ComicsList } from "../../components/details/ComicsList"
import { StoriesList } from "../../components/details/StoriesList"
import { Layaout } from "../../components/Layaout"
import { getFetch } from "../../helpers/getFetch"

const CharacterDetail = ({ params }) => {
  const id = params.id
  const [char, setchar] = useState({})
  useEffect(() => {
    getFetch(3, id).then(resp => {
      const data = resp.find(char => char.id === Number(id))
      if (!data) {
        // history.push('/404');
      } else {
        setchar(data)
      }
    })
  }, [id])
  const renderDescription = () => {
    return char.description ? (
      <p className="text-muted">{char.description}</p>
    ) : (
      <p className="text-muted"> there is no description available </p>
    )
  }
  const renderComics = () => {
    return <ComicsList id={id} category="characters" />
  }
  const renderStories = () => {
    return <StoriesList id={id} category="characters" />
  }
  const addDefaultSrc = ev => {
    ev.target.src = "/img/marvel-placeholder.jpg"
  }
  return (
    <Layaout>
      <div className="row no-gutters mt-5">
        <div className="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
          <img
            className="img-thumbnail animate__animated animate__fadeInDown"
            onError={addDefaultSrc}
            src={char.img_grande}
            alt={char.name}
          />
        </div>
        <div className="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
          <h3>{char.name}</h3>
          <hr />
          {renderDescription()}
          <hr />
          <h3>Comics</h3>
          {renderComics()}
          <hr />
          <h3>Stories</h3>
          {renderStories()}
        </div>
      </div>
    </Layaout>
  )
}

export default CharacterDetail
