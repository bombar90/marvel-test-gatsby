import React from "react"
import Banner from "../components/Banner"
import { Layaout } from "../components/Layaout"
import { MarvelList } from "../components/layout/MarvelList"
import { useFetch } from "../hooks/useFetch"

export default function Comics() {
  const { data: comics } = useFetch(2)
  return (
    <Layaout>
      <Banner />
      <h2 className="mt-5 text-center">Marvel Comics</h2>
      <MarvelList category="comics" data={comics} />
    </Layaout>
  )
}
