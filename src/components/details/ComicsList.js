import React, { useRef } from "react"
import { useFetchDetails } from "../../hooks/useFetchDetails"
import InfiniteScroll from "react-infinite-scroll-component"
import { useFecthScroll } from "../../hooks/useFecthScroll"
import { Link } from "gatsby"

export const ComicsList = ({ id, category }) => {
  const uri = `https://gateway.marvel.com:443/v1/public/${category}/${id}/comics`
  const { data: comics } = useFetchDetails(uri)
  const { items, hasMore, fetchData } = useFecthScroll(comics, uri)
  const ref = useRef(null)

  const render = () => {
    if (items.length > 0) {
      return (
        <div id="scroll-comic" ref={ref} className="row scroll__scroll-detail">
          <InfiniteScroll
            dataLength={items.length}
            next={fetchData}
            hasMore={hasMore}
            loader={<h4>Loading...</h4>}
            scrollableTarget="scroll-comic"
          >
            {items.map((comic, index) => (
              <div
                className="col-3 mb-2 animate__animated animate__fadeInUp"
                key={`${comic.id}-${index}`}
              >
                <img
                  className="img-thumbnail"
                  src={comic.thumbnail}
                  alt={comic.name}
                />
                <div>
                  <Link
                    className="text-muted pointer"
                    to={`/comics/${comic.id}`}
                  >
                    {" "}
                    {comic.name}{" "}
                  </Link>
                </div>
              </div>
            ))}
          </InfiniteScroll>
        </div>
      )
    } else {
      return <p className="text-muted"> No Comics available</p>
    }
  }
  return render()
}
