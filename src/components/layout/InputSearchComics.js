import React, { useEffect } from "react"
import { getSearchComic } from "../../helpers/getSearchComic"
import { getSearchComicFormat } from "../../helpers/getSearchComicFormat"
import { useForm } from "../../hooks/useForm"

export const InputSearchComics = ({ setItems, sethasMore, uri, data }) => {
  const [values, handleInputChange, reset] = useForm({
    search: "",
    filterType: "titleStartsWith",
    formatType: "noFilter",
  })
  const { search, filterType, formatType } = values

  useEffect(() => {
    if (formatType !== "noFilter") {
      getSearchComicFormat(uri, formatType, "format").then(resp => {
        setItems(resp)
        sethasMore(false)
      })
    }
  }, [formatType, setItems, sethasMore, uri])

  const handleSubmit = async e => {
    e.preventDefault()
    console.log(search, filterType)
    if (search) {
      const searchItems = await getSearchComic(uri, search, filterType)
      setItems(searchItems)
      sethasMore(false)
    }
  }
  const handleReset = () => {
    reset({ search: "", filterType: "titleStartsWith", formatType: "noFilter" })
    sethasMore(true)
    setItems(data)
  }

  const renderInput = () =>
    filterType === "format" ? (
      <select
        className="form-control form-select"
        aria-label="Default select example"
        onChange={handleInputChange}
        name="formatType"
        value={formatType}
      >
        <option value="noFilter">Select a format option</option>
        <option value="comic">Comic</option>
        <option value="magazine">Magazine</option>
        <option value="Trade paperback">Trade paperback</option>
        <option value="hardcover">Hardcover</option>
        <option value="digest">Digest</option>
        <option value="graphic novel">Graphic novel</option>
        <option value="digital comic">Digital comic</option>
        <option value="infinite comic">Infinite comic</option>
      </select>
    ) : (
      <input
        type="text"
        className="form-control"
        autoComplete="off"
        placeholder="Search your comic"
        value={search}
        name="search"
        onChange={handleInputChange}
      />
    )

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <div className="input-group">
          {renderInput()}
          <div className="input-group-append">
            <span className="input-group-text" id="basic-addon2">
              <select
                className="form-select"
                aria-label="Default select example"
                onChange={handleInputChange}
                name="filterType"
                value={filterType}
              >
                <option value="titleStartsWith">Title</option>
                <option value="format">Format</option>
                <option value="issueNumber">Issue Number</option>
              </select>
            </span>
          </div>
          <div className="input-group-append">
            <span className="input-group-text" id="basic-addon2">
              <button
                type="button"
                className="btn btn-danger"
                onClick={handleReset}
              >
                <i className="fas fa-redo-alt"></i>
              </button>
            </span>
          </div>
        </div>
      </form>
    </div>
  )
}
