import React from "react"
import { Link } from "gatsby"
import { useDispatch, useSelector } from "react-redux"
import { types } from "../../types/types"

export const MarvelCard = ({ name, thumbnail, id, category, description }) => {
  const { favoritos } = useSelector(state => state.fav)
  const dispatch = useDispatch()

  const renderThumbmail = () =>
    thumbnail ? (
      <img
        src={thumbnail}
        onError={addDefaultSrc}
        alt={name}
        className="card-img-top"
      />
    ) : (
      <img
        src="/img/marvel-placeholder.jpg"
        alt={name}
        className="card-img-top"
      />
    )
  const addDefaultSrc = ev => {
    ev.target.src = "/img/no-image.jpg"
  }
  const handleFavorites = () => {
    const exist = favoritos.find(fav => fav.id === id)
    if (exist) {
      dispatch({
        type: types.delFavorites,
        payload: {
          name,
          thumbnail,
          id,
          category,
          description,
        },
      })
    } else {
      dispatch({
        type: types.setFavorites,
        payload: {
          name,
          thumbnail,
          id,
          category,
          description,
        },
      })
    }
  }
  return (
    <div className="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-2">
      <div className="card m-2 animate__animated animate__fadeIn mb-2">
        {renderThumbmail()}
        <div className="card-body">
          <h3 className="card-title text-truncate">{name}</h3>
          <div className="btn-group" role="group" aria-label="Basic example">
            <Link to={`/${category}/${id}`} className="btn btn-danger">
              View
            </Link>
            <button className="btn btn-danger" onClick={handleFavorites}>
              <i className="fas fa-heart"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}
