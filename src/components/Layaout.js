import React, { Fragment, useEffect } from "react"
import { useSelector } from "react-redux"
import { Navbar } from "./Navbar"

export const Layaout = ({ children }) => {
  const { favoritos } = useSelector(state => state.fav)
  useEffect(() => {
    localStorage.setItem("favoritos", JSON.stringify(favoritos))
  }, [favoritos])

  return (
    <Fragment>
      <Navbar />
      <div className="container">{children}</div>
    </Fragment>
  )
}
