import React from "react"
import { Provider } from "react-redux"
import { createStore, combineReducers } from "redux"
import { FavoritesReducer } from "../context/FavoritesReducer"

const reducers = combineReducers({
  fav: FavoritesReducer,
})

const store = createStore(reducers)

const wrapperRedux = ({ element }) => {
  return <Provider store={store}>{element}</Provider>
}

export default wrapperRedux
