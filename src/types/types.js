export const types = {
  setFavorites: "[fav] setFavorites",
  delFavorites: "[fav] deleteFavorites",
  updateFavorites: "[fav] updateFavorites",
}
