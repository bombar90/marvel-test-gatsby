import { apiKey, ts, hash } from "./apiCredentials"

export const getFetchDetails = async url => {
  const resp = await fetch(
    `${encodeURI(url)}?apikey=${encodeURI(apiKey)}&ts=${encodeURI(
      ts
    )}&hash=${encodeURI(hash)}`
  )
  const { data } = await resp.json()
  const heroes = data.results.map(hero => {
    return {
      id: hero.id,
      name: hero.name ? hero.name : hero.title,
      description: hero.description,
      thumbnail: hero.thumbnail
        ? hero.thumbnail.path + "/portrait_xlarge.jpg"
        : "/img/marvel-placeholder.jpg",
      img_grande: hero.thumbnail
        ? hero.thumbnail.path + "/portrait_uncanny.jpg"
        : "/img/marvel-placeholder.jpg",
    }
  })

  return heroes
}
